# A Postagger, Lemmatizer, and Parser for Latin

This folder contains the code necessary to automatically postag, lemmatize, and
parse Latin using COMBO. More precisely, here you find:


1.   COMBO parser (this has been downloaded at https://github.com/360er0/COMBO). It requires Tensorflow 1.x (not compatible with 2.x).
2.   A model trained on the Latin Dependency Treebank (LDT) 2.1 + the code/files necessary
     to make the training reproducible. For the commands, see `command.txt`. 
     The corpus division into train/dev/test data has been done via `01_to-conllu.xq`
     (80% train, 10% dev, 10% test)
3.  A REST API for the model described at point 2 (i.e., the LDT data in their original annotation scheme)
4.  A REST API for UD la_perseus (model downloaded at http://mozart.ipipan.waw.pl/~prybak/model_conll2018/model.la_perseus.pkl)
5.  A REST API for UD la_ittb (model downloaded at http://mozart.ipipan.waw.pl/~prybak/model_conll2018/model.la_ittb.pkl)
6.  A REST API for UD la_proiel (model downloaded at http://mozart.ipipan.waw.pl/~prybak/model_conll2018/model.la_proiel.pkl)

I used COMBO parser, which was among the best parsers for the UD Latin data
at CoNLL 2018 Shared Task (http://universaldependencies.org/conll18/results.html). 
I am grateful to the authors of COMBO for
being among the few who made their code available: this allowed me to retrain 
COMBO on the original data of the AGLDT and so help my work go on faster.

# Accuracy

The following is the COMBO output for the accuracies on the test data (point 2 above): 

Predict 2019-02-20 22:45:05 <br/>
UAS: 0.6322775263951734 <br/>
LAS: 0.5215686274509804 <br/>
LEMMA: 0.830316742081448 <br/>
POS: 0.9081447963800905 <br/>
XPOS: 0.7205128205128205 <br/>
FEAT: 0.7487179487179487 <br/>
SEM: 1.0 <br/>
EM: 0.0 <br/>

For the accuracies of the UD models consult http://universaldependencies.org/conll18/results.html.

# REST APIs

The REST APIs can be run typing:

`>> cd COMBO_for_Latin/COMBO-ldt`<br/>
`>> python3 run_keras_server.py`

`>> cd COMBO_for_Latin/COMBO-ud-la_perseus`<br/>
`>> python3 run_keras_server.py`

`>> cd COMBO_for_Latin/COMBO-ud-la_ittb`<br/>
`>> python3 run_keras_server.py`

`>> cd COMBO_for_Latin/COMBO-ldt-la_proiel`<br/>
`>> python3 run_keras_server.py`


The REST APIs can be accessed at:

`>> cd COMBO_for_Latin/COMBO-ldt`<br/>
`>> curl -F text="@fileTempProva.conllu" -X POST 'http://localhost:5000/predict-ldt'`

`>> cd COMBO_for_Latin/COMBO-ud-la_perseus`<br/>
`>> curl -F text="@fileTempProva.conllu" -X POST 'http://localhost:5001/predict-ud-la_perseus'`

`cd COMBO_for_Latin/COMBO-ud-la_ittb`<br/>
`curl -F text="@fileTempProva.conllu" -X POST 'http://localhost:5002/predict-ud-la_ittb'`

`>> cd COMBO_for_Latin/COMBO-ldt-la_proiel`<br/>
`>> curl -F text="@fileTempProva.conllu" -X POST 'http://localhost:5003/predict-ud-la_proiel'`

I took inspiration for the APIs from https://github.com/jrosebr1/simple-keras-rest-api

# Licence

The license for COMBO parser and the UD models is CC BY-NC-SA 4.0 (see https://github.com/360er0/COMBO).

Likewise, my contribution is released under a share-alike licence, which means 
(as the licence specifies) that further data which are created/build upon the 
present data *must* also be released under the same licence. For commercial use,
ask more information.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
<img alt="Creative Commons License" style="border-width:0" 
src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />
This work is licensed under a <a rel="license" 
href="http://creativecommons.org/licenses/by-nc/4.0/">
Creative Commons Attribution-NonCommercial 4.0 International License</a>.
