import numpy as np
import flask
from flask import Flask, request
import os
import random
import numpy as np
import tensorflow as tf
from optparse import OptionParser
from sklearn.externals import joblib
from keras import backend as K
from parser import Parser
from utils import (ConllLoader,ConllSaver)
from werkzeug import secure_filename
from random import randint

app = flask.Flask(__name__)

modelPath = "model.la_ittb.pkl"
savePath = "temporaryFiles/"


os.environ['PYTHONHASHSEED'] = '0'
np.random.seed(123)
random.seed(123)
session_conf = tf.ConfigProto(intra_op_parallelism_threads=16, 
								inter_op_parallelism_threads=16)

from keras import backend as K
tf.set_random_seed(123)
sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
K.set_session(sess)

parser = OptionParser()

parser = joblib.load(modelPath)
graph = tf.get_default_graph()

loader = ConllLoader()
saver = ConllSaver()

# curl -F text="@fileTempProva.conllu" -X POST 'http://localhost:5002/predict-ud-la_ittb'
@app.route("/predict-ud-la_ittb", methods=["POST"])
def predict1():

	if flask.request.method == "POST":
		file = flask.request.files["text"]
		filename = secure_filename(file.filename)

		s = randint(0, 12000000)
		savePath2 = savePath + filename + str(s)
		file.save(savePath2)

		with graph.as_default():
			test_data = loader.load(savePath2)
			os.remove(savePath2)
			columns = [
			'id',
			'form',
			'lemma',
			'upostag',
			'xpostag',
			'feats',
			'head',
			'deprel',
			'deps',
			'misc',
			]

			pred = parser.predict(test_data)
			final = ""
			for tree in pred:
				tree_output = []
				tree_output += tree.comments
				for token in sorted(
					tree.words + tree.tokens[1:], 
					key=lambda x: float(x.fields['id'].split('-')[0]),
				):
					line_output = []
					for col in columns:
						line_output.append(token.fields.get(col, '_'))
					tree_output.append('\t'.join(line_output))
				final = final + '\n'.join(tree_output) + '\n\n'


		return flask.Response(final + '\n', mimetype='text/csv')


if __name__ == "__main__":
	print(("* Loading Keras model and Flask starting server..."
		"please wait until server has fully started"))
	app.run(host='0.0.0.0', port=5002)
	app.run()
